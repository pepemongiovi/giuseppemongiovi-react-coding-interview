import { Form, Input } from 'antd';
import React from 'react'

interface PersonEditableInfoProps {
  isEditing: boolean;
  name: string;
  label: string;
  type?: string;
}

const PersonEditableInfo = ({ isEditing, label, name, type = "text" }: PersonEditableInfoProps) => {
  return (
    <Form.Item
      label={label}
      name={name}
      rules={[{ required: true, message: `Please input your ${label}!` }]}
    >
      <Input type={type} />
    </Form.Item>
  )
}

export default PersonEditableInfo