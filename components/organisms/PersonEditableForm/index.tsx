import { Form } from "antd";
import React from "react";
import { Person } from "../../../constants/types";
import PersonEditableInfo from "../../molecules/PersonEditableInfo";

interface PersonEditableFormProps {
  person: Person;
  isEditing: boolean;
  onSubmit: () => void;
}

const PersonEditableForm = ({ person, isEditing, onSubmit }: PersonEditableFormProps) => {
  console.log(person)
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ name: person.name, gender: person.gender, phone: person.phone, birthday: person.birthday }}
      onFinish={onSubmit}
      autoComplete="off"
    >
      <PersonEditableInfo label="Name" name="name" isEditing={isEditing} />
      <PersonEditableInfo label="Gender" name="gender" isEditing={isEditing} />
      <PersonEditableInfo label="Phone" name="phone" isEditing={isEditing} />
      <PersonEditableInfo label="Birthday" name="birthday" type="date" isEditing={isEditing} />
    </Form>
  )
}

export default PersonEditableForm